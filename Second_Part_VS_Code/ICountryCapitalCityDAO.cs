﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson30_hw
{
    interface ICountryCapitalCityDAO
    {
        object GetCountryAndItsCapitalCityName(int country_id);
        object GetCountryAndItsCapitalCityDetails(int country_id);
        object GetCountryAndItsCapitalCityName(string country_name);
        object GetCountryAndItsCapitalCityDetails(string country_name);
    }
}
