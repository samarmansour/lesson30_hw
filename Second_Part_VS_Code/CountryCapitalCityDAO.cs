﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson30_hw
{
    class CountryCapitalCityDAO: ICountryCapitalCityDAO
    {
        private string m_conn_str;
        public CountryCapitalCityDAO(string conn_string)
        {
            m_conn_str = conn_string;
        }
        public object GetCountryAndItsCapitalCityName(int country_id)
        {
            using (SqlCommand cmd = new SqlCommand($"SELECT * FROM Country JOIN CapitalCity ON Country.countryID = CapitalCity.country_ID " +
                                                    $"WHERE Country.ID = {country_id}"))
            {
                using (cmd.Connection = new SqlConnection(m_conn_str))
                {
                    cmd.Connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Country CurrentCountry = new Country
                            {
                                countryID = (int)reader["ID"],
                                countryName = (string)reader["NAME"],
                                size_km = (int)reader["SIZE_KM"],
                                birth_year = (int)reader["BIRTH_YEAR"],
                                capitalCity_ID = (int)reader["CAPITALCITY_ID"]
                            };

                            CapitalCity CurrentCity = new CapitalCity
                            {
                                capitalCity_ID = (int)reader["ID"],
                                capitalCityName = (string)reader["NAME"],
                                numCitizens = (int)reader["NUMCITIZENS"],
                                country_ID = (int)reader["COUNTRY_ID"]
                            };

                            var result = new
                            {
                                Country_Name = CurrentCountry.countryName,
                                Capital_Name = CurrentCity.capitalCityName
                            };
                            return result;
                        }
                    }
                }
            }
            return null;
        }

        public object GetCountryAndItsCapitalCityDetails(int country_id)
        {
            using (SqlCommand cmd = new SqlCommand($"SELECT * FROM Country JOIN CapitalCity ON Country.countryID = CapitalCity.country_ID " +
                                                    $"WHERE Country.ID = {country_id}"))
            {
                using (cmd.Connection = new SqlConnection(m_conn_str))
                {
                    cmd.Connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Country CurrentCountry = new Country
                            {
                                countryID = Convert.ToInt32(reader["ID"]),
                                countryName = reader["NAME"].ToString(),
                                size_km = Convert.ToInt32(reader["SIZE_KM"]),
                                birth_year = Convert.ToInt32(reader["BIRTH_YEAR"]),
                                capitalCity_ID = Convert.ToInt32(reader["CAPITALCITY_ID"])
                            };

                            CapitalCity CurrentCity = new CapitalCity
                            {
                                capitalCity_ID = Convert.ToInt32(reader["ID"]),
                                capitalCityName = reader["NAME"].ToString(),
                                numCitizens = Convert.ToInt32(reader["NUMCITIZENS"]),
                                country_ID = Convert.ToInt32(reader["COUNTRY_ID"])
                            };

                            var result = new
                            {
                                Country_Name = CurrentCountry.countryName,
                                Capital_Name = CurrentCity.capitalCityName,
                                CitizensNumber = CurrentCity.numCitizens,
                                CountryID= CurrentCity.country_ID
                            };
                            return result;
                        }
                    }
                }
            }
            return null;
        }

        public object GetCountryAndItsCapitalCityName(string country_name)
        {
            using (SqlCommand cmd = new SqlCommand($"SELECT * FROM Country JOIN CapitalCity ON Country.countryID = CapitalCity.country_ID " +
                                                    $"WHERE Country.NAME = {country_name}"))
            {
                using (cmd.Connection = new SqlConnection(m_conn_str))
                {
                    cmd.Connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Country CurrentCountry = new Country
                            {
                                countryID = (int)reader["ID"],
                                countryName = (string)reader["NAME"],
                                size_km = (int)reader["SIZE_KM"],
                                birth_year = (int)reader["BIRTH_YEAR"],
                                capitalCity_ID = (int)reader["CAPITALCITY_ID"]
                            };

                            CapitalCity CurrentCity = new CapitalCity
                            {
                                capitalCity_ID = (int)reader["ID"],
                                capitalCityName = (string)reader["NAME"],
                                numCitizens = (int)reader["NUMCITIZENS"],
                                country_ID = (int)reader["COUNTRY_ID"]
                            };

                            var result = new
                            {
                                Country_Name = CurrentCountry.countryName,
                                Capital_Name = CurrentCity.capitalCityName
                            };
                            return result;
                        }
                    }
                }
            }
            return null;
        }

        public object GetCountryAndItsCapitalCityDetails(string country_name)
        {
            using (SqlCommand cmd = new SqlCommand($"SELECT * FROM Country JOIN CapitalCity ON Country.countryID = CapitalCity.country_ID " +
                                                    $"WHERE Country.NAME = {country_name}"))
            {
                using (cmd.Connection = new SqlConnection(m_conn_str))
                {
                    cmd.Connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Country CurrentCountry = new Country
                            {
                                countryID = (int)reader["ID"],
                                countryName = (string)reader["NAME"],
                                size_km = (int)reader["SIZE_KM"],
                                birth_year = (int)reader["BIRTH_YEAR"],
                                capitalCity_ID = (int)reader["CAPITALCITY_ID"]
                            };

                            CapitalCity CurrentCity = new CapitalCity
                            {
                                capitalCity_ID = (int)reader["ID"],
                                capitalCityName = (string)reader["NAME"],
                                numCitizens = (int)reader["NUMCITIZENS"],
                                country_ID = (int)reader["COUNTRY_ID"]
                            };

                            var result = new
                            {
                                Country_Name = CurrentCountry.countryName,
                                Capital_Name = CurrentCity.capitalCityName,
                                CitizensNumber = CurrentCity.numCitizens,
                                CountryID = CurrentCity.country_ID
                            };
                            return result;
                        }
                    }
                }
            }
            return null;
        }

        public List<object> GetCountryAndItsCapitalCityByFirstLetter(string firstLetter)
        {
            List<object> list = new List<object>();
            using (SqlCommand cmd = new SqlCommand($"SELECT * FROM Country JOIN CapitalCity ON Country.countryID = CapitalCity.country_ID " +
                                                    $"WHERE Country.NAME Like {firstLetter}%"))
            {
                using (cmd.Connection = new SqlConnection(m_conn_str))
                {
                    cmd.Connection.Open();
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            Country CurrentCountry = new Country
                            {
                                countryID = (int)reader["ID"],
                                countryName = (string)reader["NAME"],
                                size_km = (int)reader["SIZE_KM"],
                                birth_year = (int)reader["BIRTH_YEAR"],
                                capitalCity_ID = (int)reader["CAPITALCITY_ID"]
                            };

                            CapitalCity CurrentCity = new CapitalCity
                            {
                                capitalCity_ID = (int)reader["ID"],
                                capitalCityName = (string)reader["NAME"],
                                numCitizens = (int)reader["NUMCITIZENS"],
                                country_ID = (int)reader["COUNTRY_ID"]
                            };

                            var result = new
                            {
                                Country_Id = CurrentCountry.countryID,
                                Country_Name = CurrentCountry.countryName,
                                Capital_Name = CurrentCity.capitalCityName
                            };

                            list.Add(result);
                        }
                    }
                }
            }
            return list;
        }
    }
}
