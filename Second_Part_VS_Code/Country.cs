﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson30_hw
{
    class Country
    {
        public int countryID { get; set; }
        public string countryName { get; set; }
        public int size_km { get; set; }
        public int birth_year { get; set; }
        public int capitalCity_ID { get; set; }

        public Country()
        {
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
