﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson30_hw
{
    class CapitalCity
    {
        public int capitalCity_ID { get; set; }
        public string capitalCityName { get; set; }
        public int numCitizens { get; set; }
        public int country_ID { get; set; }

        public CapitalCity()
        {
        }

        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }
    }
}
